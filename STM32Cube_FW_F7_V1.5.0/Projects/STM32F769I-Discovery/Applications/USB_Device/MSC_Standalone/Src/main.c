/**
  ******************************************************************************
  * @file    USB_Device/MSC_Standalone/Src/main.c
  * @author  MCD Application Team
  * @version V1.0.1
  * @date    23-September-2016
  * @brief   USB device Mass storage demo main file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2016 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
unsigned long esp32_limit_time;
unsigned char leds_sd, usb_initctrl;
unsigned char tryMountAgain;

USBD_HandleTypeDef USBD_Device;
FATFS SDFatFs;  /* File system object for SD card logical drive */
FIL MyFile;     /* File object */
char SDPath[4]; /* SD card logical drive path */

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);
static void CPU_CACHE_Enable(void);

#define RXBUFFERSIZE 1
#define DATARXBUFSIZE 4096
#define FILENAMESIZE 256

char aRxBuffer[RXBUFFERSIZE];

uint8_t new_file_to_burn;
uint32_t bufindex;

uint32_t filedatasize;
uint32_t filenamesize;
char filedata_buffer[DATARXBUFSIZE];
char filename_buffer[FILENAMESIZE];
uint32_t brnfiledatasize;
uint32_t brnfilenamesize;
char brnfiledata_buffer[DATARXBUFSIZE];
char brnfilename_buffer[FILENAMESIZE];

typedef enum
{
	receiving_start_filename,
	receiving_size_filename,
	receiving_data_filename,
	receiving_stop_filename,
	receiving_start_filedata,
	receiving_size_filedatamsb,
	receiving_size_filedatalsb,
	receiving_data_filedata,
	receiving_stop_filedata,
	end_of_reception
} state_t;

state_t myState;

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  FRESULT res;                            /* FatFs function common result code */
  uint32_t byteswritten;                     /* File write/read counts */

  leds_sd=0;
  tryMountAgain=0;
  usb_initctrl=1;

  /* Enable the CPU Cache */
  CPU_CACHE_Enable();

  /* STM32F7xx HAL library initialization:
       - Configure the Flash ART accelerator on ITCM interface
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
  HAL_Init();

  /* Configure the System clock to have a frequency of 216 MHz */
  SystemClock_Config();

  /* Configure LED1 and LED2 */
  BSP_LED_Init(LED1);
  BSP_LED_Init(LED2);

  MX_USART6_UART_Init();
  /* USER CODE BEGIN 2 */

  HAL_UART_Receive_IT(&huart6, (uint8_t *)aRxBuffer, RXBUFFERSIZE);
  new_file_to_burn = 0;
  myState=receiving_start_filename;

  esp32_limit_time=5000;

  /*##-1- Link the micro SD disk I/O driver ##################################*/
  if(FATFS_LinkDriver(&SD_Driver, SDPath) == 0)
  {
//      /* Create FAT volume */
//      res = f_mkfs(SDPath, 0, 4096);
//      if (res != FR_OK)
//      {
//        Error_Handler();
//      }
	  /*##-2- Register the file system object to the FatFs module ##############*/
	  if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
	  {
		  /* FatFs Initialization Error */
		  tryMountAgain = 1;
	  }
	  /* Run Application (Interrupt mode) */
	  while (1)
	  {
		  if(tryMountAgain)
		  {
			  /*##-2- Register the file system object to the FatFs module ##############*/
			  if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
			  {
				  /* FatFs Initialization Error */
				  tryMountAgain = 1;
			  }
			  else
			  {
				  tryMountAgain = 0;
			  }
		  }
		  if(!tryMountAgain)
		  {
			  if (!esp32_limit_time)
			  {
				  if(usb_initctrl)
				  {
					  usb_initctrl=0;
					  /* Init Device Library */
					  USBD_Init(&USBD_Device, &MSC_Desc, 0);

					  /* Add Supported Class */
					  USBD_RegisterClass(&USBD_Device, USBD_MSC_CLASS);

					  /* Add Storage callbacks for MSC Class */
					  USBD_MSC_RegisterStorage(&USBD_Device, &USBD_DISK_fops);

					  /* Start Device Process */
					  USBD_Start(&USBD_Device);
				  }
			  }
			  else if(myState!=end_of_reception)
			  {
				  if(new_file_to_burn)
				  {
					  /* Create and Open a new text file object with write access */
					  if(f_open(&MyFile, brnfilename_buffer, FA_CREATE_ALWAYS | FA_WRITE) == FR_OK)
					  {
						  /* Write data to the text file */
						  res = f_write(&MyFile, brnfiledata_buffer, brnfiledatasize, (void *)&byteswritten);

						  if((byteswritten > 0) && (res == FR_OK))
						  {
							  /* Close the open text file */
							  f_close(&MyFile);
						  }
					  }
					  new_file_to_burn=0;
				  }
			  }
		  }
	  }
  }
  else
  {
	  Error_Handler();
  }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 216000000
  *            HCLK(Hz)                       = 216000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLLSAI_N                       = 384
  *            PLLSAI_P                       = 8
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 432;  
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  
  /* Activate the OverDrive to reach the 216 Mhz Frequency */
  if(HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  
  /* Select PLLSAI output as USB clock source */
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLLSAIP;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 384;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 7; 
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV8;
  if(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct)  != HAL_OK)
  {
    Error_Handler();
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {
  }
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

/**
  * @brief  Toggle LEDs to show user input state.   
  * @param  None
  * @retval None
  */
void Toggle_Leds(void)
{
	static uint32_t ticks;

	if(ticks++ == 100)
	{
		if(leds_sd==2)
		{
			BSP_LED_Toggle(LED1);
			BSP_LED_Off(LED2);
		}
		else if(leds_sd==1)
		{
			BSP_LED_Toggle(LED2);
			BSP_LED_Off(LED1);
		}
		ticks = 0;
	}
}

/**
  * @brief This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */

  /* USER CODE END USART6_IRQn 0 */
  HAL_UART_IRQHandler(&huart6);
  /* USER CODE BEGIN USART6_IRQn 1 */

  /* USER CODE END USART6_IRQn 1 */
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	switch (myState)
	{
	case end_of_reception:
		break;

	case receiving_start_filename:
		if(aRxBuffer[0]==0xCA)
		{
			esp32_limit_time+=1000;
			myState=receiving_size_filename;
		}
		else if(aRxBuffer[0]==0xC9)
		{
			myState=end_of_reception;
		}
		break;

	case receiving_size_filename:
		filename_buffer[0]='\0';
		if(aRxBuffer[0]<FILENAMESIZE)
		{
			filenamesize = aRxBuffer[0];
			myState=receiving_data_filename;
			bufindex=0;
		}
		else
		{
			myState=receiving_start_filename;
		}
		break;

	case receiving_data_filename:
		if(bufindex<filenamesize)
		{
			if(aRxBuffer[0]=='/')
			{
				filenamesize--;
			}
			else
			{
				filename_buffer[bufindex] = aRxBuffer[0];
				bufindex++;
			}
		}
		else if((bufindex==filenamesize) && (aRxBuffer[0]=='\0'))
		{
			filename_buffer[bufindex] = aRxBuffer[0];
			myState=receiving_stop_filename;
		}
		else
		{
			myState=receiving_start_filename;
		}
		break;

	case receiving_stop_filename:
		if(aRxBuffer[0]==0xC6)
		{
			myState=receiving_start_filedata;
		}
		else
		{
			myState=receiving_start_filename;
		}
		break;

	case receiving_start_filedata:
		if(aRxBuffer[0]==0xDA)
		{
			myState=receiving_size_filedatamsb;
		}
		else
		{
			myState=receiving_start_filename;
		}
		break;

	case receiving_size_filedatamsb:
		filedatasize = (aRxBuffer[0] << 8) & 0xFF00;
		myState=receiving_size_filedatalsb;
		break;

	case receiving_size_filedatalsb:
		filedata_buffer[0]='\0';
		filedatasize = filedatasize | (aRxBuffer[0] & 0xFF);
		if(filedatasize<DATARXBUFSIZE)
		{
			myState=receiving_data_filedata;
			bufindex=0;
		}
		else
		{
			myState=receiving_start_filename;
		}
		break;

	case receiving_data_filedata:
		if(bufindex<filedatasize)
		{
			filedata_buffer[bufindex] = aRxBuffer[0];
			bufindex++;
		}
		else if((bufindex==filedatasize) && (aRxBuffer[0]=='\0'))
		{
			myState=receiving_stop_filedata;
		}
		else
		{
			myState=receiving_start_filename;
		}
		break;

	case receiving_stop_filedata:
		if(aRxBuffer[0]==0xD6)
		{
			new_file_to_burn = 1;
			brnfiledatasize = filedatasize;
			brnfilenamesize = filenamesize;
			memcpy(brnfilename_buffer, filename_buffer, brnfilenamesize+1);
			memcpy(brnfiledata_buffer, filedata_buffer, brnfiledatasize+1);
		}
		myState=receiving_start_filename;
		break;
	}
	HAL_UART_Receive_IT(&huart6, (uint8_t *)aRxBuffer, RXBUFFERSIZE);
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
